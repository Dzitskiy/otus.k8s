using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using Microsoft.AspNetCore.Mvc;

namespace ver1.Controllers;

[ApiController]
[Route("[controller]")]
[Route("")]
public class HomeController : ControllerBase
{
    private readonly ILogger<WeatherForecastController> _logger;

    public HomeController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    [HttpGet]
    public ActionResult<HomeInfo> Get()
    {
        return new HomeInfo();
    }
}

public class HomeInfo
{
    static string _ip = GetPhysicalIPAdress();
    static string _name = GetName();

    public string Ip => _ip;
    public string Name => _name;

    public static string GetName()
    {
        return $"{Dns.GetHostName()} / {System.Environment.MachineName}";
    }

    public static string GetPhysicalIPAdress()
    {
        foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
        {
            var addr = ni.GetIPProperties().GatewayAddresses.FirstOrDefault();
            if (addr != null && !addr.Address.ToString().Equals("0.0.0.0"))
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            return ip.Address.ToString();
                        }
                    }
                }
            }
        }
        return String.Empty;
    }
}
