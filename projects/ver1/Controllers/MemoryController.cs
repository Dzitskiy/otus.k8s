using Microsoft.AspNetCore.Mvc;

namespace ver1.Controllers;

[ApiController]
[Route("[controller]")]
public class MemoryController : ControllerBase
{
    private readonly ILogger<MemoryController> _logger;

    public MemoryController(ILogger<MemoryController> logger)
    {
        _logger = logger;
    }

    public IActionResult Get()
    {
        _logger.LogInformation("new chunk");
        OutOfMemory.AddNewChunk();

        return Ok(new
        {
            memory = OutOfMemory.GetSize().ToString("N2"),
            name = HomeInfo.GetName()
        });
    }
}

public static class OutOfMemory
{
    const int _def = 3 * 1024 * 1024;
    static List<byte[]> _bytes = new List<byte[]>();

    public static void AddNewChunk(int l = 0)
    {
        _bytes.Add(new byte[l > 0 ? l : _def]);
    }

    public static long GetSize()
    {
        return _bytes.Sum(b => b.Length);
    }
}
